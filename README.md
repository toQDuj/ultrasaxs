# README #

This is a collection of files and drawings for making an Ultra-SAXS instrument. The instrument is designed and released under a CC-BY license. 

Upgrades and design improvements are always welcome. The author can be contacted via his website: http://lookingatnothing.com/