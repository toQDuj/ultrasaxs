include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 10; // height of bases
overBite = 20; // countersink heights
bHoleRaise = -0; //raise bSunk holes by this much. 

//a little more tolerance in the screw holes:
M6R1 = 3.25; // through-hole radius
M6R2 = 6.3; // countersink radius
M4R1 = 2.25; // through-hole radius
M4R2 = 4.3; // countersink radius
outerRM6 = 12;

//Hole positions:
// RA10A-W mounting holes, M4:
OH1 = [30.0, 30.0, 0];
OH2 = [-30.0, 30.0, 0];
OH3 = [30.0, -30.0, 0];
OH4 = [-30.0, -30.0, 0];
//GNL10/M holes, M2.5:
KH1 = [  16.4,  16.4, 0]; 
KH2 = [ -16.4,  16.4, 0]; 
KH3 = [  16.4, -16.4, 0]; 
KH4 = [ -16.4, -16.4, 0]; 

difference() {
	// base material:
	union(){
		roundedRect(70, 70, bHeight, 3);
		//reinforcement around the PR01 holes:
		tR4(OH1, bHeight);
		tR4(OH2, bHeight);
		tR4(OH3, bHeight);
		tR4(OH4, bHeight);
		// GNL10 reinforcements
		bR2p5(KH1, bHeight);
		bR2p5(KH2, bHeight);
		bR2p5(KH3, bHeight);
		bR2p5(KH4, bHeight);
	}
	// material saving holes
	cylinder ( h = bHeight *2, r = 10, center = true, $fn = cDiv);
	translate([45, 0, 0])
	cylinder ( h = bHeight *2, r = 25, center = true, $fn = cDiv);
	translate([-45, 0, 0])
	cylinder ( h = bHeight *2, r = 25, center = true, $fn = cDiv);
	translate([0, 45, 0])
	cylinder ( h = bHeight *2, r = 25, center = true, $fn = cDiv);
	translate([0, -45, 0])
	cylinder ( h = bHeight *2, r = 25, center = true, $fn = cDiv);
	// Kohzu holes
	tM2p5(KH1, bHeight + .05);
	tM2p5(KH2, bHeight + .05);
	tM2p5(KH3, bHeight + .05);
	tM2p5(KH4, bHeight + .05);

	// M6 XT95 table holes
	bM4(OH1, bHeight + .05);
	bM4(OH2, bHeight + .05);
	bM4(OH3, bHeight + .05);
	bM4(OH4, bHeight + .05);
}