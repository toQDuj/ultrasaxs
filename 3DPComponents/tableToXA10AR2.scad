include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 10; // height of bases
overBite = 20; // countersink heights
bHoleRaise = -0; //raise bSunk holes by this much. 

//a little more tolerance in the screw holes:
M6R1 = 3.25; // through-hole radius
M6R2 = 6.3; // countersink radius
M4R1 = 2.25; // through-hole radius
M4R2 = 4.3; // countersink radius
outerRM6 = 12;

//Hole positions:
// optical table holes, M6 
OH1 = [150/2, 150/2, -2];
OH2 = [-150/2, 150/2, -2];
OH3 = [150/2, -150/2, -2];
OH4 = [-150/2, -150/2, -2];
//KohzuHoles, M4:
KH1 = [90/2, 90/2, 0]; 
KH2 = [-90/2, 90/2, 0]; 
KH3 = [90/2, -90/2, 0]; 
KH4 = [-90/2, -90/2, 0]; 

difference() {
	// base material:
	union() {
		rotate([0,0,45])
		cube([ 24, 210, bHeight], center = true);
		rotate([0,0,45])
		cube([ 210, 24, bHeight], center = true);
		cube([ 130, 130, bHeight], center = true);
		cylinder( h = bHeight *1.001, r = 45*sqrt(2) + 10, center = true, $fn = cDiv);
	//reinforcement around the optical table holes:
	tR6(OH1, bHeight + 4);
	tR6(OH2, bHeight + 4);
	tR6(OH3, bHeight + 4);
	tR6(OH4, bHeight + 4);
	// kohzu reinforcements
	bR4(KH1, bHeight);
	bR4(KH2, bHeight);
	bR4(KH3, bHeight);
	bR4(KH4, bHeight);
	}
	// material saving holes
	translate(KH1 /2)
	cylinder ( h = bHeight *2, r = 15, center = true, $fn = cDiv);
	translate(KH2 /2)
	cylinder ( h = bHeight *2, r = 15, center = true, $fn = cDiv);
	translate(KH3 /2)
	cylinder ( h = bHeight *2, r = 15, center = true, $fn = cDiv);
	translate(KH4 /2)
	cylinder ( h = bHeight *2, r = 15, center = true, $fn = cDiv);
	cylinder ( h = bHeight *2, r = 8, center = true, $fn = cDiv);
	translate([52, 0, 0])
	cylinder ( h = bHeight *2, r = 12, center = true, $fn = cDiv);
	translate([-52, 0, 0])
	cylinder ( h = bHeight *2, r = 12, center = true, $fn = cDiv);
	translate([0, 52, 0])
	cylinder ( h = bHeight *2, r = 12, center = true, $fn = cDiv);
	translate([0, -52, 0])
	cylinder ( h = bHeight *2, r = 12, center = true, $fn = cDiv);
	// Kohzu holes
	tM4(KH1, bHeight + .05);
	tM4(KH2, bHeight + .05);
	tM4(KH3, bHeight + .05);
	tM4(KH4, bHeight + .05);

	// M6 optical table holes
	bM6(OH1, bHeight + 4);
	bM6(OH2, bHeight + 4);
	bM6(OH3, bHeight + 4);
	bM6(OH4, bHeight + 4);
}