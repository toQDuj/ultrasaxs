cDiv = 100;
bHeight = 10;
cHeight = 20; 

module tSunkM6() {
	// central hole
	cylinder( h = bHeight + 1, r = 3.1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, bHeight / 2 - 2.99])
	cylinder( h = 6, r = 6.2, center = true, $fn = cDiv);
}

module bSunkM4() {
	// central hole
	cylinder( h = bHeight + 1, r = 2.1, center = true, $fn = cDiv);
	// countersink bottom
	translate([0, 0, -bHeight / 2. + 1.99])
		cylinder( h = 4, r = 4.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -bHeight / 2. + 0.5 + 3.99])
		cylinder( h = 1, r1 = 4.1, r2 = 2.1, center = true, $fn = cDiv);
}

module tSunkM4() {
	// central hole
	cylinder( h = bHeight + 1, r = 2.1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, bHeight / 2 - 1.99])
	cylinder( h = 4, r = 4.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, bHeight / 2. - 0.5 - 3.99])
		cylinder( h = 1, r1 = 2.1, r2 = 4.1, center = true, $fn = cDiv);
}

module vSlit() {
	// slit dimensions: 19.95, 1.8, 54
	// zero is at beam height:
	translate([-10.05, -2, -20])
	union() {
		difference() {
			// first attempt: a bit tight:
			// cube([20.1, 1.9, 55]);
			cube([20.2, 2.0, 55]);
			// beveled edge on the side, 45 degrees
			//rotate([0, 0, 45])
			//translate([0, 0.71, 0])
			//cube([2, 2, 55]);
		}
	// additional cut-outs for surface-mounted structure:
		translate([2, 1.9, 2])
		cube([16, 2, 53]);
		translate([4, 1.9, 5])
		cube([12, 5, 50]);
	}	
}	 

module hSlit() {
	// like vSlit, but with one side beveled to prevent overhang in printing
    // slit dimensions: 19.95, 1.8, 54
	// zero is at beam height:
	translate([-10.05, -2, -20])
	union() {
		difference() {
			cube([20.2, 2.0, 55]);
			// beveled edge on the side, 45 degrees
			rotate([0, 0, 45])
			translate([0, 0.71, 0])
			cube([2, 2, 55]);
		}
		// additional cut-outs for surface-mounted structure:
		translate([2, 1.9, 2])
		cube([16, 2, 53]);
		translate([4, 1.9, 5])
		cube([12, 5, 50]);
		//extra bevel
		translate([2.2, 0.4, 0])
		rotate([0, 0, 55])
		cube([6.5, 2, 50]);
		
	}	
}	 


difference() {
	// base material:
	union(){
		cube([60, 60, bHeight], center = true);
		translate([2.5, 0, bHeight / 2 + cHeight])
		cube([55, 30, 40], center = true);
	}
	// beam hole
	translate([0, 0, cHeight + bHeight /2])
	rotate([90,0,0]) // KF25 hole = 1.04 in, 26.42 mm
		cylinder(h = 40, r = 13.21, center = true, $fn = cDiv);
	translate([0, 4, cHeight + bHeight /2])
		vSlit();
	translate([0, -4, cHeight + bHeight / 2])
	rotate([180, 90, 0])
		hSlit();

	//KF mounts
	translate([18.3, 0, bHeight / 2 + cHeight - 14.3])
	rotate([90, 0, 0])
		cylinder(h = 40, r = 2.2, center = true, $fn = cDiv);
	translate([-18.3, 0, bHeight / 2 + cHeight - 14.3])
	rotate([90, 0, 0])
		cylinder(h = 40, r = 2.2, center = true, $fn = cDiv);
	translate([16.3, 0, bHeight / 2 + cHeight + 16.3])
	rotate([90, 0, 0])
		cylinder(h = 40, r = 2.2, center = true, $fn = cDiv);
	translate([-16.3, 0, bHeight / 2 + cHeight + 16.3])
	rotate([90, 0, 0])
		cylinder(h = 40, r = 2.2, center = true, $fn = cDiv);
	
	// Corner
	translate([30, 15, bHeight / 2 + 22.5 + .1])
	rotate([0, 0, 45])
	 	cube([15, 15, 45], center = true);

	// M4 holes
	translate([25, 25, 0])
		tSunkM4();
	translate([25, -25, 0])
		tSunkM4();
	translate([-25, 25, 0])
		tSunkM4();
	translate([-25, -25, 0])
		tSunkM4();

}