cDiv = 100;
bHeight = 10; // height of bases
overBite = 20; // countersink heights
bHoleRaise = -4; //raise bSunk holes by this much. 

//for reinforcements
outerR = 12;
innerR = 4.5;
taperHeight = 5;

// screw holes:
M6R1 = 3.15;
M6R2 = 6.2;

//Hole positions:
// optical table holes 
OH1 = [125/2, 125/2, 0];
OH2 = [-125/2, 125/2, 0];
OH3 = [125/2, -125/2, 0];
OH4 = [-125/2, -125/2, 0];

module bSunkM6() {
	// central hole
	cylinder( h = bHeight + 15, r = M6R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, -overBite /2 - 1 ])
	cylinder( h = overBite, r = M6R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -0.5 ])
		cylinder( h = 1, r1 = M6R2, r2 = M6R1, center = true, $fn = cDiv);
}

module tSunkM6() {
	// central hole
	cylinder( h = bHeight + 1, r = M6R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = M6R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = M6R1, r2 = M6R2, center = true, $fn = cDiv);
}

module tReinforce() {
	// for reinforcing bolt holes
	cylinder(r = outerR, h = bHeight * 1.0, center = true, $fn = cDiv);
	translate([0, 0, -bHeight /2 - taperHeight /2])
	cylinder(r2 = outerR, r1 = innerR, h = taperHeight, center = true, $fn = cDiv);
}


difference() {
	// base material:
	union() {
		rotate([0,0,45])
		cube([ 24, 180, bHeight], center = true);
		rotate([0,0,45])
		cube([ 180, 24, bHeight], center = true);
		cube([ 95, 95, bHeight], center = true);
		cylinder( h = bHeight *1.001, r = 100/2, center = true, $fn = cDiv);
	//reinforcement around the optical table holes:
	translate(OH1)
	tReinforce();
	translate(OH2)
	tReinforce();
	translate(OH3)
	tReinforce();
	translate(OH4)
	tReinforce();
	}
	// material saving holes
	cylinder ( h = bHeight *2, r = 27.5, center = true, $fn = cDiv);
	translate([0, 140/2, 0])
	cylinder ( h = bHeight *2, r = 17.5, center = true, $fn = cDiv);
	translate([0, -140/2, 0])
	cylinder ( h = bHeight *2, r = 17., center = true, $fn = cDiv);
	// M6 holes
	translate([75/2, 12.5, 0])
		tSunkM6();	
	translate([75/2, -12.5, 0])
		tSunkM6();	
	translate([-75/2, 12.5, 0])
		tSunkM6();	
	translate([-75/2, -12.5, 0])
		tSunkM6();
	translate([12.5, 75/2, 0])
		tSunkM6();	
	translate([-12.5, 75/2, 0])
		tSunkM6();	
	translate([12.5, -75/2, 0])
		tSunkM6();	
	translate([-12.5, -75/2, 0])
		tSunkM6();	
	// M6 optical table holes
	translate([125/2, 125/2, bHoleRaise])
		bSunkM6();
	translate([-125/2, 125/2, bHoleRaise])
		bSunkM6();
	translate([125/2, -125/2, bHoleRaise])
		bSunkM6();
	translate([-125/2, -125/2, bHoleRaise])
		bSunkM6();
}