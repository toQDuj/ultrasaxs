include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 13; // height of bases
tHeight = 70; // total height
pHeight = tHeight - 2* bHeight; // central post height
oDim = 60; // outer dimension
overBite = 20; // countersink heights
XTbHeight = 13; // height of clamp mount base
outerRM4 = 7;
taperHeightM4 = 6;
 
// four holes in top
EH1t = [25, 25, tHeight /2 - bHeight /2 - 2]; 
EH2t = [-25, 25, tHeight /2 - bHeight /2 - 2];
EH3t = [25, -25, tHeight /2 - bHeight /2 - 2];
EH4t = [-25, -25, tHeight /2 - bHeight /2 - 2];
// four holes in bottom; skip
//EH1b = [25, 25, -tHeight /2 + bHeight /2 + 2]; 
//EH2b = [-25, 25, -tHeight /2 + bHeight /2 + 2];
//EH3b = [25, -25, -tHeight /2 + bHeight /2 + 2];
//EH4b = [-25, -25, -tHeight /2 + bHeight /2 + 2];
// four more holes for XT95P11/m mount
XH1b = [75/2, 12.5, -tHeight /2 + XTbHeight /2];
XH2b = [-75/2, 12.5, -tHeight /2 + XTbHeight /2];
XH3b = [75/2, -12.5, -tHeight /2 + XTbHeight /2];
XH4b = [-75/2, -12.5, -tHeight /2 + XTbHeight /2];

module XT95P11Adapter(XTbHeight = XTbHeight){
	difference() {
		// base material:
		intersection() {
			cube([ 95, 60, XTbHeight], center = true);
			cylinder( h = XTbHeight *2, r = 95/2, center = true, $fn = cDiv);
		}
	}
}

module baseMat() {
	// Here, we add reinforcements and a bigger base to allow addition
	// of XT95 clamp holes.
	translate([0,0,-tHeight / 2 + XTbHeight /2])
	XT95P11Adapter(XTbHeight);
	translate(EH1t)
	tReinforceM4(bHeight+ 4);			
	translate(EH2t)
	tReinforceM4(bHeight + 4);			
	translate(EH3t)
	tReinforceM4(bHeight + 4);			
	translate(EH4t)
	tReinforceM4(bHeight + 4);			
	translate(EH1b)
	bReinforceM4(bHeight + 4);			
	translate(EH2b)
	bReinforceM4(bHeight + 4);			
	translate(EH3b)
	bReinforceM4(bHeight + 4);			
	translate(EH4b)
	bReinforceM4(bHeight + 4);			

	// This is the taper connecting the XT plate to the post
	intersection(){
		cube([oDim + 40 , oDim, tHeight], center = true);
		translate([0, 0, -pHeight /2 + bHeight /2])
		cylinder(h = bHeight * 1.0, r1 = sqrt(2) * oDim /2 , r2 = oDim /2, center = true, $fn = cDiv);

	}
	// This is the post structure itself
	intersection(){
		cube([oDim, oDim, tHeight], center = true);
		cylinder(h = tHeight, r = 41, center = true, $fn = cDiv);
		union(){
			// bottom plate:
			translate([0, 0, -pHeight / 2 - bHeight /2])
			cube([oDim, oDim, bHeight], center = true);
			// top plate:
			translate([0, 0, pHeight / 2 + bHeight /2])
			cube([oDim, oDim, bHeight], center = true);
			//reinforcement around screw holes
			// central post:
			cylinder(h = pHeight, r = oDim /2, center = true, $fn = cDiv);
			// connecting cylinder structures
			translate([0, 0, -pHeight /2 + bHeight /2])
			cylinder(h = bHeight * 1, r1 = sqrt(2) * oDim /2 , r2 = oDim /2, center = true, $fn = cDiv);
			translate([0, 0, pHeight /2 - bHeight / 2])
			cylinder(h = bHeight * 1, r2 = sqrt(2) * oDim /2 , r1 = oDim /2, center = true, $fn = cDiv);
		}
	}
}

difference() {
	baseMat();
	// weight saving hole
	cylinder(h = tHeight + 1, r = oDim /2 - 10 , center = true, $fn = cDiv);
	// top holes
	translate(EH1t)
	bSunkM4(bHeight + 5, overBite);
	translate(EH2t)
	bSunkM4(bHeight + 5, overBite);
	translate(EH3t)
	bSunkM4(bHeight + 5, overBite);
	translate(EH4t)
	bSunkM4(bHeight + 5, overBite);
	// bottom holes
	translate(EH1b)
	tSunkM4(bHeight + 5, overBite);
	translate(EH2b)
	tSunkM4(bHeight + 5, overBite);
	translate(EH3b)
	tSunkM4(bHeight + 5, overBite);
	translate(EH4b)
	tSunkM4(bHeight + 5, overBite);
//XT95P11/m holes:
	// M6 holes
	translate(XH1b)
	tSunkM6(XTbHeight + 1, overBite);	
	translate(XH2b)
	tSunkM6(XTbHeight + 1, overBite);	
	translate(XH3b)
	tSunkM6(XTbHeight + 1, overBite);	
	translate(XH4b)
	tSunkM6(XTbHeight + 1, overBite);
}

