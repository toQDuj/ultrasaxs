include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 10; // height of bases
overBite = 20; // countersink heights

//a little more tolerance in the screw holes:
M6R1 = 3.25; // through-hole radius
M6R2 = 6.3; // countersink radius
M4R1 = 2.25; // through-hole radius
M4R2 = 4.3; // countersink radius
outerRM6 = 12;

//Hole positions:
// RA10A-W mounting holes, M4:
OH1 = [30.0, 30.0, 0];
OH2 = [-30.0, 30.0, 0];
OH3 = [30.0, -30.0, 0];
OH4 = [-30.0, -30.0, 0];
//GNL10/M holes, M2.5:
KH1 = [  16.4,  16.4, 0]; 
KH2 = [ -16.4,  16.4, 0]; 
KH3 = [  16.4, -16.4, 0]; 
KH4 = [ -16.4, -16.4, 0]; 

module tM6Slot() {
	roundedRect(6.3, 30, bHeight +.1, 3);
	translate([0, 0, bHeight /2 + .1])
	roundedRect(12.3, 37, bHeight +.1, 6);
}

module triangle(w, h, bHeight, r) {
	//translate([0, 0, bHeight /2])
	intersection(){
		rotate([0, 0, 45])
		roundedRect(sqrt(2) * w, sqrt(2) * h, bHeight, r);
		translate([0, 0, -bHeight/2])
		cube([w, h, bHeight]);
	}
}

difference() {
	// base material:
	union(){
		translate([0, -10, 0])
		roundedRect(60, 100, bHeight, 3);
		translate([0, 35, 30])
		rotate([90, 0, 0])
		roundedRect(60, 65, bHeight, 3);

		//supports:
		translate([0, 30, bHeight /2])
		rotate([0, -90, 180])
		triangle(60, 60, bHeight, 5);

		translate([-25, 30, bHeight /2])
		rotate([0, -90, 180])
		triangle(25, 25, bHeight, 5);

		translate([25, 30, bHeight /2])
		rotate([0, -90, 180])
		triangle(25, 25, bHeight, 5);

		//reinforcement around the PR01 holes:
		//tR4(OH1, bHeight);
	}
	// base slots for M6 mounting:
	translate([-12.5, 10, 0])
	tM6Slot();
	translate([-12.5, -30, 0])
	tM6Slot();
	translate([12.5, 10, 0])
	tM6Slot();
	translate([12.5, -30, 0])
	tM6Slot();
	
	// M6 holes for kinematic base:
	translate([0, 35, 35])
	rotate([90, 0, 0]){
	tM6([12.5, 12.5, 0], bHeight + .05);
	tM6([-12.5, 12.5, 0], bHeight + .05);
	tM6([12.5, -12.5, 0], bHeight + .05);
	tM6([-12.5, -12.5, 0], bHeight + .05);
	}

}