include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 101;
bHeight = 10; // height of bases
bHoleRaise = -0; //raise bSunk holes by this much. 
tHoleRaise = 0; //raise tSunk holes by this much. 

//Hole positions:
// RA-10Z holes 
tH1 = [-90/2 * sqrt(2), 0, bHoleRaise];
tH2 = [90/2 * sqrt(2), 0, bHoleRaise];
tH3 = [0, -90/2 * sqrt(2), bHoleRaise];
tH4 = [0, 90/2 * sqrt(2), bHoleRaise];

//jack holes
bH1 = [50, 50, bHoleRaise];
bH2 = [-50, 50, bHoleRaise];
bH3 = [50, -50, bHoleRaise];
bH4 = [-50, -50, bHoleRaise];

bH5 = [0, 0, bHoleRaise];
bH6 = [-25, 50, bHoleRaise];
bH7 = [25, 50, bHoleRaise];

bH8 = [50, 25, bHoleRaise];
bH9 = [-50, 25, bHoleRaise];

difference() {
	// base material:
	union() {
	roundedRect(140, 140, bHeight, 25);
	//reinforcement around the holes:
	tR4(tH1, bHeight);
	tR4(tH2, bHeight);
	tR4(tH3, bHeight);
	tR4(tH4, bHeight);

	bR4(tH1, bHeight);
	bR4(tH2, bHeight);
	bR4(tH3, bHeight);
	bR4(tH4, bHeight);

	}
	// material saving cut-out
	translate([27, -25, 0])
	cylinder(r = 19, h = bHeight +1 , center = true, $fn = cDiv);
	translate([-27, -25, 0])
	cylinder(r = 19, h = bHeight +1 , center = true, $fn = cDiv);
	translate([22, 22, 0])
	cylinder(r = 15, h = bHeight +1 , center = true, $fn = cDiv);
	translate([-22, 22, 0])
	cylinder(r = 15, h = bHeight +1 , center = true, $fn = cDiv);
	translate([0, 46, 0])
	cylinder(r = 8, h = bHeight +1 , center = true, $fn = cDiv);
	translate([0, -47, 0])
	cylinder(r = 7, h = bHeight +1 , center = true, $fn = cDiv);
	translate([46, 5, 0])
	cylinder(r = 7, h = bHeight +1 , center = true, $fn = cDiv);
	translate([-46, 5, 0])
	cylinder(r = 7, h = bHeight +1 , center = true, $fn = cDiv);
	//translate([95/2 + 25, 0, 0])
	//cube([100, 100, bHeight +1], center = true);

	// RA-10Z holes
	tM4(tH1, bHeight + 1, bHeight /2 +1);	
	tM4(tH2, bHeight + 1, bHeight /2 +1);	
	tM4(tH3, bHeight + 1, bHeight /2 +1);	
	tM4(tH4, bHeight + 1, bHeight /2 +1);	
	
	bM4(bH1, bHeight + 1, bHeight /2 +1);	
	bM4(bH2, bHeight + 1, bHeight /2 +1);	
	bM4(bH3, bHeight + 1, bHeight /2 +1);	
	bM4(bH4, bHeight + 1, bHeight /2 +1);	
	bM4(bH5, bHeight + 1, bHeight /2 +1);	
	bM4(bH6, bHeight + 1, bHeight /2 +1);	
	bM4(bH7, bHeight + 1, bHeight /2 +1);	
	bM4(bH8, bHeight + 1, bHeight /2 +1);	
	bM4(bH9, bHeight + 1, bHeight /2 +1);	
}