cDiv = 100;
bHeight = 10;
EH1 = [16.3, 16.3, 0];
EH2 = [-14.3, 18.3, 0];
EH3 = [16.3, -16.3, 0];
EH4 = [-14.3, -18.3, 0];

module tSunkM4() {
	// central hole
	cylinder( h = bHeight + 1, r = 2.2, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, bHeight / 2 - 1.99])
	cylinder( h = 4, r = 4.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, bHeight / 2. - 0.5 - 3.99])
		cylinder( h = 1, r1 = 2.1, r2 = 4.1, center = true, $fn = cDiv);
}

difference() {
	// base material:
	union(){
		cylinder(h = bHeight, r = 26, center = true, $fn = cDiv);
		//edge holes:
		translate(EH1)
			cylinder(h = bHeight, r = 6, center = true, $fn = cDiv);
		translate(EH2)
			cylinder(h = bHeight, r = 6, center = true, $fn = cDiv);
		translate(EH3)
			cylinder(h = bHeight, r = 6, center = true, $fn = cDiv);
		translate(EH4)
			cylinder(h = bHeight, r = 6, center = true, $fn = cDiv);
	}
	// bottom flat 
	translate([-43 / 2 - 7 /2, 0, 0])
		cube([7, 43, bHeight + .1], center = true);
	// outer bore 
	translate([0, 0, bHeight / 2 - 3.1 / 2])
		cylinder(h = 3.11, r = 21, center = true, $fn = cDiv);
	// cone section 
	translate([0, 0, bHeight / 2 - 3.1 - 1.34 /2 + 0.01 ])
		cylinder(h = 1.34, r1 = 16, r2 = 21, center = true, $fn = cDiv);
	
	// central bore
	translate([0, 0, 0])
	rotate([0, 0, 0]) 
		cylinder(h = bHeight+2, r = 16, center = true, $fn = cDiv);
	// side slot	
	translate([-14, 0, 0])
	rotate([0, 0, 0]) 
		cube([28, 28, bHeight + .1], center = true);
	// Bottom limit
	translate([-29.5, 0, 0])
		cube([20, 50, bHeight + .1], center = true);
	// edge holes
	translate(EH1)
	rotate([0, 0, 0])
		cylinder(h = bHeight + 2, r = 2.2, center = true, $fn = cDiv);
	translate(EH2)
	rotate([0, 0, 0])
		cylinder(h = bHeight + 2, r = 2.2, center = true, $fn = cDiv);
	translate(EH3)
	rotate([0, 0, 0])
		cylinder(h = bHeight + 2, r = 2.2, center = true, $fn = cDiv);
	translate(EH4)
	rotate([0, 0, 0])
		cylinder(h = bHeight + 2, r = 2.2, center = true, $fn = cDiv);
}

