include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 15; // height of bases
bHoleRaise = -0; //raise bSunk holes by this much. 
tHoleRaise = 0; //raise tSunk holes by this much. 

//Hole positions:
// optical table holes 
tH1 = [-95/2 + 85, 140/2, tHoleRaise];
tH2 = [-95/2 + 85 , -140/2, tHoleRaise];
bH1 = [-95/2 + 16, -178/2, bHoleRaise];
bH2 = [-95/2 + 16, 178/2, bHoleRaise];
bH3 = [-95/2 + 56, -178/2, bHoleRaise];
bH4 = [-95/2 + 56, 178/2, bHoleRaise];
eH0 = [-95/2 + 75, 0, 0]; // 6.5 unthreaded
eH1 = [-95/2 + 10, -25, 0]; // 4.5 unthreaded
eH2 = [-95/2 + 10, 25, 0]; // 4.5 unthreaded
eH3 = [-95/2 + 35, -50, 0]; // 4.5 unthreaded
eH4 = [-95/2 + 35, 50, 0]; // 4.5 unthreaded


difference() {
	// base material:
	union() {
		cube([ 95, 189 , bHeight], center = true);
		//translate([-95/2, 0, 0])
		//cube([ 15, 75, bHeight], center = true);
	//reinforcement around the optical table holes:
	translate(tH1)
	tReinforceM6(bHeight);
	translate(tH2)
	tReinforceM6(bHeight);
	translate(bH1)
	bReinforceM6(bHeight);
	translate(bH2)
	bReinforceM6(bHeight);
	translate(bH3)
	bReinforceM6(bHeight);
	translate(bH4)
	bReinforceM6(bHeight);
	translate(eH1)
	cylinder(r = 10, h = bHeight, center = true, $fn = cDiv);
	translate(eH2)
	cylinder(r = 10, h = bHeight, center = true, $fn = cDiv);

	}
	// Right side block:
	translate([-70, -10, -(bHeight + 20)/2])
	cube([140, 200, bHeight + 20]);
	// material saving cut-out
	translate([25, 0, 0])
	cylinder(r = 50, h = bHeight +1 , center = true, $fn = cDiv);
	translate([95/2 + 25, 0, 0])
	cube([100, 100, bHeight +1], center = true);
	// M6 holes
	translate(tH1)
		tSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(tH2)
		tSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(bH1)
		bSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(bH2)
		bSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(bH3)
		bSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(bH4)
		bSunkM6(bHeight + 1, bHeight /2 +1);	
	// extra holes:
	translate(eH0)
		tSunkM6(bHeight + 1, bHeight /2 +1);	
	translate(eH1)
		tSunkM4(bHeight + 1, bHeight /2 +1);	
	translate(eH2)
		tSunkM4(bHeight + 1, bHeight /2 +1);	
	translate(eH3)
		tSunkM4(bHeight + 1, bHeight /2 +1);	
	translate(eH4)
		tSunkM4(bHeight + 1, bHeight /2 +1);	

}