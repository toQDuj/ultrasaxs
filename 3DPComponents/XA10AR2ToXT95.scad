include <holesAndReinforcements.scad> //defining M4 and M6
cDiv = 100;
bHeight = 15; // height of bases
overBite = 20; // countersink heights
bHoleRaise = -0; //raise bSunk holes by this much. 

//a little more tolerance in the screw holes:
M6R1 = 3.25; // through-hole radius
M6R2 = 6.3; // countersink radius
M4R1 = 2.25; // through-hole radius
M4R2 = 4.3; // countersink radius
outerRM6 = 12;

//Hole positions:
// XT95 inner four holes, M6, offset to allow access to kohzu holes:  
halfOffset = 8; 
OH1 = [ 84/2,  84/2 + halfOffset, 0];
OH2 = [-84/2,  84/2 + halfOffset, 0];
OH3 = [ 84/2, -84/2 + halfOffset, 0];
OH4 = [-84/2, -84/2 + halfOffset, 0];
//KohzuHoles, M4:
KH1 = [ 90/2, 90/2 - halfOffset, 0]; 
KH2 = [-90/2, 90/2 - halfOffset, 0]; 
KH3 = [ 90/2,-90/2 - halfOffset, 0]; 
KH4 = [-90/2,-90/2 - halfOffset, 0]; 

difference() {
	// base material:
	union(){
		roundedRect(100, 120, bHeight, 3);
		//reinforcement around the optical table holes:
		tR6(OH1, bHeight);
		tR6(OH2, bHeight);
		tR6(OH3, bHeight);
		tR6(OH4, bHeight);
		// kohzu reinforcements
		bR4(KH1, bHeight);
		bR4(KH2, bHeight);
		bR4(KH3, bHeight);
		bR4(KH4, bHeight);
	}
	// material saving holes
	translate([0, halfOffset, 0])
	cylinder ( h = bHeight *2, r = 30, center = true, $fn = cDiv);
	// Kohzu holes
	tM4(KH1, bHeight + .05);
	tM4(KH2, bHeight + .05);
	tM4(KH3, bHeight + .05);
	tM4(KH4, bHeight + .05);

	// M6 XT95 table holes
	bM6(OH1, bHeight + .05);
	bM6(OH2, bHeight + .05);
	bM6(OH3, bHeight + .05);
	bM6(OH4, bHeight + .05);
}