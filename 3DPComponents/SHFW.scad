cDiv = 100;
bHeight = 10; // height of bases
overBite = 15; // countersink heights

// four holes for mounting
EH1 = [25, 25, 0 * bHeight]; 
EH2 = [-25, 25, 0 * bHeight];
EH3 = [25, -25, 0 * bHeight];
EH4 = [-25, -25, 0 * bHeight];

module tSunkM4() {
	// central hole
	cylinder( h = bHeight + 1, r = 2.1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = 4.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = 2.1, r2 = 4.2, center = true, $fn = cDiv);
}

module bSunkM6() {
	// central hole
	cylinder( h = bHeight + .1, r = 3.15, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, -overBite /2 - 1 ])
	cylinder( h = overBite, r = 6.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -0.5 ])
		cylinder( h = 1, r1 = 6.2, r2 = 3.15, center = true, $fn = cDiv);
}

module tSunkM6() {
	// central hole
	cylinder( h = bHeight + .1, r = 3.15, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = 6.2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = 3.15, r2 = 6.2, center = true, $fn = cDiv);
}

module baseMat() {
	// base material:
	translate([10, 0, 0])
		cube([80, 60, bHeight], center = true);
}

difference(){
	baseMat();
	// material saving hole
	cylinder( h = bHeight + .1, r = 15, center = true, $fn = cDiv);
	// four mounting holes for base
	translate(EH1)
	tSunkM4();
	translate(EH2)
	tSunkM4();
	translate(EH3)
	tSunkM4();
	translate(EH4)
	tSunkM4();
	// mounts for filter wheel
	translate([25, 8.8, 0])
	bSunkM6();
	translate([39.8, 8.8, 0])
	bSunkM6();
	// mount for shutter
	translate([31.7, -12.2, 0])
	bSunkM6();
}