// module defining countersunk holes and their optional reinforcements
// defines holes for M6 and M4 hex-head bolts

// hole radii definitions: 
M6R1 = 3.15; // through-hole radius
M6R2 = 6.2; // countersink radius
M4R1 = 2.15; // through-hole radius
M4R2 = 4.2; // countersink radius
M2p5R1 = 1.55; // through-hole radius
M2p5R2 = 3.5; // countersink radius

// total height of countersunk structure (half is bolt width), usually
// overridden by bHeight in file:
coreHeight = 15; //total height of core structure, must be >= 10
overBite = coreHeight /2 -1; //additional top height to countersinks

//for M6 reinforcements
outerRM6 = 10;
innerRM6 = 4.5;
taperHeightM6 = 4;

//for M4 reinforcements
outerRM4 = 8;
innerRM4 = 2.5;
taperHeightM4 = 4;

//for M2.5 reinforcements
outerRM2p5 = 6.5;
innerRM2p5 = 1.5;
taperHeightM2p5 = 4;

// super definitions:
// M2.5
module bR2p5(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	bReinforceM2p5(coreHeight);
	}

module tR2p5(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	tReinforceM2p5(coreHeight);
	}

module bM2p5(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	bSunkM2p5(coreHeight, overBite = overBite);	
	}

module tM2p5(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	tSunkM2p5(coreHeight, overBite);	
	}

// M4
module bR4(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	bReinforceM4(coreHeight);
	}

module tR4(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	tReinforceM4(coreHeight);
	}

module bM4(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	bSunkM4(coreHeight, overBite = overBite);	
	}

module tM4(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	tSunkM4(coreHeight, overBite);	
	}

// M6
module bR6(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	bReinforceM6(coreHeight);
	}

module tR6(pos, coreHeight = coreHeight) {
	//bottom reinforcements
	translate(pos)
	tReinforceM6(coreHeight);
	}

module bM6(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	bSunkM6(coreHeight, overBite = overBite);	
	}

module tM6(pos, coreHeight = coreHeight, overBite = overBite) {
	translate(pos)
	tSunkM6(coreHeight, overBite);	
	}

// base definitions
module roundedRect(xSize, ySize, bHeight = coreHeight, cRadius) {
	// defines a rounded rectangle for use in (f.ex.) baseplates
	xSideClip = xSize - 2 * cRadius;
	ySideClip = ySize - 2 * cRadius;
	union() {
		cube([xSize, ySideClip, bHeight], center = true);
		cube([xSideClip, ySize, bHeight], center = true);
		translate([xSideClip / 2, ySideClip / 2, 0])
		cylinder(h = bHeight, r = cRadius, center = true, $fn = cDiv);
		translate([-xSideClip / 2, ySideClip / 2, 0])
		cylinder(h = bHeight, r = cRadius, center = true, $fn = cDiv);
		translate([xSideClip / 2, -ySideClip / 2, 0])
		cylinder(h = bHeight, r = cRadius, center = true, $fn = cDiv);
		translate([-xSideClip / 2, -ySideClip / 2, 0])
		cylinder(h = bHeight, r = cRadius, center = true, $fn = cDiv);
	}

	}
// roundedRect(100, 50, 10, 10);

module bSunkM6(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M6R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, -overBite /2 - 1 ])
	cylinder( h = overBite, r = M6R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -0.5 ])
		cylinder( h = 1, r1 = M6R2, r2 = M6R1, center = true, $fn = cDiv);
}

module tSunkM6(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M6R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = M6R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = M6R1, r2 = M6R2, center = true, $fn = cDiv);
}

module tSunkM4(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M4R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = M4R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = M4R1, r2 = M4R2, center = true, $fn = cDiv);
}

module bSunkM4(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M4R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, -overBite /2 - 1 ])
	cylinder( h = overBite, r = M4R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -0.5 ])
		cylinder( h = 1, r2 = M4R1, r1 = M4R2, center = true, $fn = cDiv);
}

module tSunkM2p5(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M2p5R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, overBite /2 + 1 ])
	cylinder( h = overBite, r = M2p5R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, 0.5 ])
		cylinder( h = 1, r1 = M2p5R1, r2 = M2p5R2, center = true, $fn = cDiv);
}

module bSunkM2p5(coreHeight = coreHeight, overBite = overBite) {
	// central hole
	cylinder( h = coreHeight, r = M2p5R1, center = true, $fn = cDiv);
	// countersink top
	translate([0, 0, -overBite /2 - 1 ])
	cylinder( h = overBite, r = M2p5R2, center = true, $fn = cDiv);
	// 1 mm very cony item
	translate([0, 0, -0.5 ])
		cylinder( h = 1, r2 = M2p5R1, r1 = M2p5R2, center = true, $fn = cDiv);
}


module tReinforceM6(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM6 = coreHeight /4;
	translate([0, 0, taperHeightM6 /2])
	cylinder(r = outerRM6, h = coreHeight * 1.0 - taperHeightM6, center = true, $fn = cDiv);
	translate([0, 0, -coreHeight /2 + taperHeightM6 /2])
	cylinder(r2 = outerRM6, r1 = innerRM6, h = taperHeightM6, center = true, $fn = cDiv);
}

module bReinforceM6(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM6 = coreHeight /4;
	translate([0, 0, -taperHeightM6 /2])
	cylinder(r = outerRM6, h = coreHeight * 1.0 - taperHeightM6, center = true, $fn = cDiv);
	translate([0, 0, +coreHeight /2 - taperHeightM6 /2])
	cylinder(r1 = outerRM6, r2 = innerRM6, h = taperHeightM6, center = true, $fn = cDiv);
}

module tReinforceM4(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM4 = coreHeight /4;
	translate([0, 0, taperHeightM4 /2])
	cylinder(r = outerRM4, h = coreHeight * 1.0 - taperHeightM4, center = true, $fn = cDiv);
	translate([0, 0, -coreHeight /2 + taperHeightM4 /2])
	cylinder(r2 = outerRM4, r1 = innerRM4, h = taperHeightM4, center = true, $fn = cDiv);
}

module bReinforceM4(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM4 = coreHeight /4;	
	translate([0, 0, -taperHeightM4 /2])
	cylinder(r = outerRM4, h = coreHeight * 1.0 - taperHeightM4, center = true, $fn = cDiv);
	translate([0, 0, +coreHeight /2 - taperHeightM4 /2])
	cylinder(r1 = outerRM4, r2 = innerRM4, h = taperHeightM4, center = true, $fn = cDiv);
}

module tReinforceM2p5(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM2p5 = coreHeight /4;
	translate([0, 0, taperHeightM2p5 /2])
	cylinder(r = outerRM2p5, h = coreHeight * 1.0 - taperHeightM2p5, center = true, $fn = cDiv);
	translate([0, 0, -coreHeight /2 + taperHeightM2p5 /2])
	cylinder(r2 = outerRM2p5, r1 = innerRM2p5, h = taperHeightM2p5, center = true, $fn = cDiv);
}

module bReinforceM2p5(coreHeight = coreHeight) {
	// for reinforcing bolt holes
	taperHeightM2p5 = coreHeight /4;	
	translate([0, 0, -taperHeightM2p5 /2])
	cylinder(r = outerRM2p5, h = coreHeight * 1.0 - taperHeightM2p5, center = true, $fn = cDiv);
	translate([0, 0, +coreHeight /2 - taperHeightM2p5 /2])
	cylinder(r1 = outerRM2p5, r2 = innerRM2p5, h = taperHeightM2p5, center = true, $fn = cDiv);
}

// tests:
module tests(){
	translate([-20, 10, 0])
	tSunkM6(); //without coreHeight and overBite arguments
	translate([-20, -10, 0])
	bSunkM6(); //with arguments
	translate([-20, 25, 0])
	tSunkM6(5, 5);
	translate([-20, -25, 0])
	bSunkM6(5, 5);
	
	translate([-7.5, 10, 0])
	tSunkM4();
	translate([-7.5, -10, 0])
	bSunkM4();
	translate([-7.5, 25, 0])
	tSunkM4(5, 5);
	translate([-7.5, -25, 0])
	bSunkM4(5, 5);
	
	translate([30, 10, 0])
	tReinforceM6();
	translate([30, -10, 0])
	bReinforceM6();
	translate([30, 30, 0])
	tReinforceM6(6);
	translate([30, -30, 0])
	bReinforceM6(6);
	translate([7.5, 10, 0])
	tReinforceM4();
	translate([7.5, -10, 0])
	bReinforceM4();
	translate([7.5, 30, 0])
	tReinforceM4(6);
	translate([7.5, -30, 0])
	bReinforceM4(6);
}
//tests();